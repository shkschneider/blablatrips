BlaBlaTrips
===========

Authors
-------

By: Alan SCHNEIDER "ShkSchneider" <shk.schneider@gmail.com>
For: Cyril CADORET <cyril.cadoret@blablacar.com>

<https://jobs.smartrecruiters.com/BlaBlaCar/743999681377272-senior-android-engineer-blablacar>

Specifications
--------------

Android: API-21+
Endpoint: https://edge.blablacar.com
ClientId: android-technical-tests
ClientSecret: Y1oAL3QdPfVhGOWj3UeDjo3q02Qwhvrj

Libraries
---------

- Kotlin + Coroutines
- Lifecycle + ViewModel + LiveData
- Material
- Fuel + Gson
- Koin

No RxJava nor Retrofit nor Dagger was used in this project (in favor or Coroutines, Fuel and Koin).
Coroutines are a nice way to work in Kotlin.
Fuel is a nice library I'm playing around laterly. I'm of course used to work with Retrofit.
Koin is a nice replacement for Dagger in Kotlin.

Architecture
------------

It has been made as a simple 3-layere onion Clean Architecure.
It is split per layer, not feature.

Beforehand, this application used a monolitic module.

Testing
-------

Testing if setup both for unit tests and instrumented/ui test.
There are only a few placeholders in place to prove the working status of the tests
but no real in-depth tests are done.

License
-------

WTFPL
