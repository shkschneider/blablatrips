package me.shkschneider.blablatrip

import me.shkschneider.blablatrips.repository.data.Distance
import me.shkschneider.blablatrips.repository.data.Duration
import me.shkschneider.blablatrips.repository.data.Place
import org.junit.Assert.assertEquals
import org.junit.Test

class RepositoryDataTest {

    @Test
    fun distance() {
        with(Distance(42, "km")) { assertEquals("42 km", toString()) }
        with(Distance(null, "km")) { assertEquals(true, toString().isNullOrBlank()) }
        with(Distance(42, null)) { assertEquals(true, toString().isNullOrBlank()) }
        with(Distance(null, null)) { assertEquals(true, toString().isNullOrBlank()) }
    }

    @Test
    fun duration() {
        with(Duration(42, "h")) { assertEquals("42 h", toString()) }
        with(Duration(null, "h")) { assertEquals(true, toString().isNullOrBlank()) }
        with(Duration(42, null)) { assertEquals(true, toString().isNullOrBlank()) }
        with(Duration(null, null)) { assertEquals(true, toString().isNullOrBlank()) }
    }

    @Test
    fun place() {
        with(Place(cityName = "City", address = "Address")) { assertEquals("Address", toString()) }
        with(Place(cityName = "City", address = null)) { assertEquals("City", toString()) }
    }

    // TODO Price

    // TODO Trip

}
