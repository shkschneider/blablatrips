package me.shkschneider.blablatrips.repository.net

import android.content.Context
import com.github.kittinunf.fuel.Fuel
import com.github.kittinunf.fuel.core.awaitResponse
import me.shkschneider.blabla.repository.R

class TokenManager(context: Context) {

    private val gson by NetworkUtils.gson()

    private val clientId = context.getString(R.string.client_id)
    private val clientSecret = context.getString(R.string.client_secret)

    // TODO could be retain in SharedPreferences
    var accessToken: String? = null

    fun headers() = NetworkUtils.headers()

    suspend fun token() =
        Fuel.post("/token")
            .header(headers())
            .body(gson.toJson(TokenRequest(clientId = clientId, clientSecret = clientSecret)))
            .awaitResponse(deserializerOf(TokenResponse::class.java)).third.also {
            accessToken = it.accessToken
        }

}
