package me.shkschneider.blablatrips.repository.net

data class TokenResponse(
    val accessToken: String? = null,
    val tokenType: String? = null,
    val issuedAt: Int? = null,
    val expiresIn: Int? = null,
    val clientId: String? = null,
    val scopes: List<String>? = null
)
