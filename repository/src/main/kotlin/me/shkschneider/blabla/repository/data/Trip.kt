package me.shkschneider.blablatrips.repository.data

data class Trip(
    val answerDelay: Int? = null,
    // arrival_passenger_routing
    val arrivalPlace: Place? = null,
    val bookingMode: String? = null,
    val bookingType: String? = null,
    // commission
    val corridoringType: String? = null,
    val departureDate: String? = null,
    // departure_passenger_routing
    val departurePlace: Place? = null,
    val distance: Distance? = null,
    val duration: Duration? = null,
    val freeway: Boolean? = null,
    val isComfort: Boolean? = null,
    // links
    private val locationsToDisplay: List<String>? = null,
    val permanentId: String? = null,
    val price: Price? = null,
    // price_with_commission
    // price_without_commission
    val seats: Int? = null,
    val seatsLeft: Int? = null,
    val tripDetailsId: String? = null,
    // user
    val viaggia_rosa: Boolean? = null
) {
    fun departureToDisplay() = locationsToDisplay?.get(0) ?: departurePlace?.cityName
    fun arrivalToDisplay() = locationsToDisplay?.get(1) ?: arrivalPlace?.cityName
}
