package me.shkschneider.blablatrips.repository.net

import me.shkschneider.blablatrips.repository.data.Trip

data class TripsResponse(
    val distance: Int? = null,
    val duration: Int? = null,
    // facets
    val fullTripsCount: Int? = null,
    // links
    val lowestPrice: Int? = null,
    // lowest_price_object
    // pager
    val recommendedPrice: Int? = null,
    val savings: Int? = null,
    val sortingAlgorithm: String? = null,
    // top_trips
    val totalTripCountToDisplay: Int? = null,
    val trips: List<Trip>? = null
)
