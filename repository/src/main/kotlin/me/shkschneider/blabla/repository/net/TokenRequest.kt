package me.shkschneider.blablatrips.repository.net

data class TokenRequest(
    val grantType: String? = "client_credentials",
    val clientId: String?,
    val clientSecret: String?,
    val scopes: List<String>? = listOf("SCOPE_TRIP_DRIVER", "SCOPE_USER_MESSAGING", "SCOPE_INTERNAL_CLIENT", "DEFAULT")
)
