package me.shkschneider.blablatrips.repository.data

data class Place(
    val cityName: String? = null,
    val address: String? = null,
    val latitude: Float? = null,
    val longitude: Float? = null
) {
    override fun toString(): String = address ?: cityName ?: ""
}
