package me.shkschneider.blablatrips.repository.net

import android.os.Build
import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.google.gson.FieldNamingPolicy
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import java.io.Reader
import java.util.Currency
import java.util.Locale

inline fun <reified T : Any> deserializerOf(clazz: Class<T>) = object : ResponseDeserializable<T> {

    private val gson by NetworkUtils.gson()

    override fun deserialize(reader: Reader): T? = gson.fromJson<T>(reader, clazz)

}

object NetworkUtils {

    fun gson(): Lazy<Gson> =
        lazy { GsonBuilder().setFieldNamingStrategy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES).create() }

    fun headers(): MutableMap<String, String> {
        return mutableMapOf(
            "Content-Type" to "application/json",
            "Accept" to "application/json",
            "Accept-Encoding" to "gzip",
            "Accept-Language" to "fr",
            "Application-Client" to "Android",
            "Application-Version" to "1.0.0", // TODO Gradle: buildConfigField
            "Connection" to "Keep-Alive",
            "X-Client" to "ANDROID|${Build.VERSION.SDK_INT}",
            "X-Currency" to Currency.getInstance(Locale.getDefault()).toString(),
            "X-Locale" to Locale.getDefault().toString()
        )
    }

}
