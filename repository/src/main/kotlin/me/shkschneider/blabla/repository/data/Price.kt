package me.shkschneider.blablatrips.repository.data

data class Price(
    val value: Int? = null,
    val currency: String? = null,
    val symbol: String? = null,
    val stringValue: String? = null,
    val priceColor: String? = null
) {
    override fun toString(): String = stringValue.orEmpty()
}
