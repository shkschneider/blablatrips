package me.shkschneider.blablatrips.repository.data

data class Duration(
    val value: Int? = null,
    val unity: String? = null
) {
    override fun toString(): String = if (value != null && unity != null) "$value $unity" else ""
}
