package me.shkschneider.blablatrips.repository.net

import com.github.kittinunf.fuel.core.Parameters
import com.google.gson.annotations.SerializedName
import java.util.Currency
import java.util.Locale

data class TripsRequest(
    @SerializedName("_format") val format: String? = "json",
    val locale: String? = Locale.getDefault().toString(),
    @SerializedName("cur") val currency: String? = Currency.getInstance(Locale.getDefault()).currencyCode,
    @SerializedName("fn") val departure: String? = null,
    @SerializedName("tn") val arrival: String? = null,
    @SerializedName("db") val date: String? = null,
    val page: Int? = null,
    val seats: Int? = null
) {

    fun toParameters(): Parameters =
        listOf(
            "_format" to format,
            "locale" to locale,
            "cur" to currency,
            "fn" to departure,
            "tn" to arrival,
            "db" to date,
            "page" to page?.toString(),
            "seats" to seats?.toString()
        )

}
