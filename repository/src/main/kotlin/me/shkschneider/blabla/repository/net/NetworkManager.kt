package me.shkschneider.blablatrips.repository.net

import android.content.Context
import com.github.kittinunf.fuel.Fuel
import com.github.kittinunf.fuel.core.FuelError
import com.github.kittinunf.fuel.core.FuelManager
import com.github.kittinunf.fuel.core.Request
import com.github.kittinunf.fuel.core.Response
import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.github.kittinunf.fuel.core.isSuccessful
import com.github.kittinunf.fuel.core.response
import me.shkschneider.blabla.repository.R
import java.util.concurrent.TimeUnit

private val TIMEOUT = TimeUnit.SECONDS.toMicros(15).toInt()

class NetworkManager(context: Context, private val tokenManager: TokenManager) {

    init {
        with(FuelManager.instance) {
            basePath = context.getString(R.string.endpoint)
            timeoutInMillisecond = TIMEOUT
            timeoutReadInMillisecond = TIMEOUT
        }
    }

    fun hasAccessToken(): Boolean = !tokenManager.accessToken.isNullOrEmpty()

    private fun headers(): Map<String, String> {
        return NetworkUtils.headers().apply {
            if (hasAccessToken()) {
                put("Authorization", "Bearer ${tokenManager.accessToken}")
            }
        }
    }

    suspend fun trips(tripsRequest: TripsRequest): Result<TripsResponse> =
        request(
            Fuel.get("/api/v2/trips", tripsRequest.toParameters()).header(headers()),
            deserializerOf(TripsResponse::class.java)
        )

    private suspend fun <T : Any> request(
        request: Request,
        deserializer: ResponseDeserializable<T>
    ): Result<T> {
        try {
            if (!hasAccessToken()) {
                return refreshToken(request, deserializer)
            } else {
                with(request.response(deserializer)) {
                    if (second.isSuccessful) {
                        return result(second, third.get())
                    } else if (second.statusCode == 401) {
                        return refreshToken(request, deserializer)
                    } else {
                        return result(second, throwable = third.component2())
                    }
                }
            }
        } catch (e: FuelError) {
            return result(e.response, throwable = e.exception)
        }
    }

    private suspend fun <T : Any> refreshToken(
        request: Request,
        deserializer: ResponseDeserializable<T>
    ): NetworkManager.Result<T> {
        tokenManager.token().also {
            with(request.header(headers()).response(deserializer)) {
                return result(second, third.get())
            }
        }
    }

    private fun <T : Any> result(response: Response, data: T? = null, throwable: Throwable? = null): Result<T> =
        Result(response.statusCode, response.responseMessage, data, throwable)

    data class Result<T : Any>(
        val statusCode: Int,
        val statusMessage: String,
        val data: T?,
        val error: Throwable?
    )

}
