package me.shkschneider.blablatrip

import androidx.test.rule.ActivityTestRule
import androidx.test.runner.AndroidJUnit4
import me.shkschneider.blablatrip.trips.TripsActivity
import org.junit.Assert
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class TripsActivityTest {

    @get:Rule
    var rule = ActivityTestRule(TripsActivity::class.java)
    private val activity by lazy { rule.activity }

    @Test
    fun activity() {
        Assert.assertTrue(activity is TripsActivity)
    }

}
