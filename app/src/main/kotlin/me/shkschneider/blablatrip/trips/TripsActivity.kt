package me.shkschneider.blablatrip.trips

import android.os.Bundle
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.AdapterView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_main.*
import me.shkschneider.blabla.usecase.TripsViewModel
import me.shkschneider.blablatrip.BuildConfig
import me.shkschneider.blablatrip.R
import me.shkschneider.blablatrip.OverlayLoader
import org.koin.androidx.viewmodel.ext.android.viewModel

class TripsActivity : AppCompatActivity() {

    private val viewModel by viewModel<TripsViewModel>()

    private var overlayLoader: OverlayLoader? = null
    private lateinit var adapter: TripsAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (BuildConfig.DEBUG) {
            departureEditText.setText("Paris")
            arrivalEditText.setText("Rennes")
        }

        arrivalEditText.setOnEditorActionListener { _, actionId, _ ->
            return@setOnEditorActionListener if (actionId == EditorInfo.IME_ACTION_SEARCH) validateAndSearch() else false
        }

        recyclerView.setHasFixedSize(true)
        recyclerView.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        adapter = TripsAdapter(itemClickListener = AdapterView.OnItemClickListener { _, view, position, _ ->
            adapter.get(position)?.let { trip ->
                Toast.makeText(view.context, trip.permanentId, Toast.LENGTH_SHORT).show()
            }
        })
        recyclerView.adapter = adapter

        floatingActionButton.setOnClickListener {
            validateAndSearch()
        }

        viewModel.trips.observe(this, Observer { result ->
            cancel()
            result?.data?.let {
                adapter.bind(it.trips)
            } ?: run {
                Toast.makeText(applicationContext, result.error!!.message, Toast.LENGTH_SHORT).show()
            }
        })
    }

    private fun validateAndSearch(): Boolean {
        val departure = departureEditText.text.toString()
        val arrival = arrivalEditText.text.toString()
        if (departure.isBlank()) {
            departureEditText.requestFocus()
            return false
        }
        if (arrival.isBlank()) {
            arrivalEditText.requestFocus()
            return false
        }
        search(departure, arrival)
        return true
    }

    private fun search(departure: String, arrival: String) {
        if (overlayLoader != null) return
        floatingActionButton.hide()
        overlayLoader = OverlayLoader.show(this)
        viewModel.trips(departure, arrival)
    }

    private fun cancel() {
        floatingActionButton.show()
        overlayLoader?.hide()
        overlayLoader = null
    }

    override fun onBackPressed() {
        if (linearLayout.visibility == View.GONE) {
            cancel()
        } else {
            super.onBackPressed()
        }
    }

}
