package me.shkschneider.blablatrip.trips

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_trip.view.*
import me.shkschneider.blablatrip.R
import me.shkschneider.blablatrips.repository.data.Trip

class TripsAdapter(
    private val itemClickListener: AdapterView.OnItemClickListener? = null,
    private val itemLongClickListener: AdapterView.OnItemLongClickListener? = null
) :
    RecyclerView.Adapter<TripsAdapter.TripsViewHolder>() {

    private var data: MutableList<Trip> = mutableListOf()

    fun bind(trips: List<Trip>?) {
        data.clear()
        trips?.let {
            data.addAll(it)
        }
        notifyDataSetChanged()
    }

    fun get(position: Int): Trip? = data[position]

    override fun getItemCount(): Int = data.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TripsViewHolder {
        return TripsViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_trip,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: TripsViewHolder, position: Int) {
        holder.itemView.setOnClickListener { view ->
            itemClickListener?.onItemClick(null, view, position, position.toLong())
        }
        holder.itemView.setOnLongClickListener { view ->
            itemLongClickListener?.onItemLongClick(null, view, position, position.toLong())
            return@setOnLongClickListener itemLongClickListener != null
        }
        with(holder.itemView) {
            data[position].let {
                departureTextView.text = it.departureToDisplay()
                arrivalTextView.text = it.arrivalToDisplay()
                dateTextView.text = it.departureDate
                priceTextView.text = it.price?.stringValue
                distanceTextView.text = it.distance?.toString().orEmpty()
                durationTextView.text = it.duration?.toString().orEmpty()
            }
        }
    }

    class TripsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

}