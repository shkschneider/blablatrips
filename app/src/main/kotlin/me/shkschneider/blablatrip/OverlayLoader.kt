package me.shkschneider.blablatrip

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import androidx.annotation.UiThread
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.DialogFragment

class OverlayLoader : DialogFragment() {

    companion object {

        @UiThread
        fun show(activity: AppCompatActivity): OverlayLoader {
            // IllegalStateException: FragmentManager is already executing transactions
            return OverlayLoader().apply {
                showNow(activity.supportFragmentManager, "OverlayLoader")
            }
        }

    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return super.onCreateDialog(savedInstanceState).also { dialog ->
            isCancelable = false
            dialog.window?.setBackgroundDrawableResource(android.R.color.transparent)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return ProgressBar(container?.context ?: context, null, android.R.attr.progressBarStyleLarge).apply {
            isIndeterminate = true
        }
    }

    @UiThread
    fun hide() {
        dismiss()
    }

}