package me.shkschneider.blablatrip

inline val Any.TAG
    get() = this.javaClass.simpleName.orEmpty()
