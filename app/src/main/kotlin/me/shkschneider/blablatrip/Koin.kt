package me.shkschneider.blablatrip

import android.app.Application
import android.content.Context
import me.shkschneider.blabla.usecase.DataManager
import me.shkschneider.blabla.usecase.TripsViewModel
import me.shkschneider.blablatrips.repository.net.NetworkManager
import me.shkschneider.blablatrips.repository.net.TokenManager
import org.koin.android.ext.android.startKoin
import org.koin.androidx.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module

class Koin(val context: Context) {

    private val uiModule = module {
        viewModel {
            TripsViewModel(get())
        }
    }
    private val useCaseModule = module {
        single {
            DataManager(get())
        }
    }
    private val repositoryModule = module {
        single {
            NetworkManager(context, get())
        }
        single {
            TokenManager(context)
        }
    }

    fun start(application: Application) {
        application.startKoin(context, listOf(uiModule, useCaseModule, repositoryModule))
    }

}
