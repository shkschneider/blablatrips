package me.shkschneider.blablatrip

import android.app.Application
import me.shkschneider.blabla.usecase.DataManager
import me.shkschneider.blabla.usecase.TripsViewModel
import me.shkschneider.blablatrips.repository.net.NetworkManager
import me.shkschneider.blablatrips.repository.net.TokenManager
import org.koin.androidx.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module

@Suppress("unused")
class BlaBlaTripsApplication : Application() {

    private val modules = listOf(
        module {
            viewModel {
                TripsViewModel(get())
            }
            single {
                DataManager(get())
            }
            single {
                NetworkManager(applicationContext, get())
            }
            single {
                TokenManager(applicationContext)
            }
        }
    )

    override fun onCreate() {
        super.onCreate()
        Koin(applicationContext).start(this)
    }

}