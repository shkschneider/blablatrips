package me.shkschneider.blabla.usecase

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import me.shkschneider.blablatrips.repository.net.TripsResponse

class TripsViewModel(private val dataManager: DataManager) : ViewModel() {

    private val _trips = MutableLiveData<DataManager.Result<TripsResponse>>()
    val trips: LiveData<DataManager.Result<TripsResponse>> get() = _trips

    fun trips(departure: String, arrival: String): LiveData<DataManager.Result<TripsResponse>> {
        Coroutines.ioThenMain({
            dataManager.trips(departure, arrival)
        }) {
            _trips.value = it
        }
        return trips
    }

}