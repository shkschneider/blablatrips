package me.shkschneider.blabla.usecase

import androidx.annotation.WorkerThread
import me.shkschneider.blablatrips.repository.net.NetworkManager
import me.shkschneider.blablatrips.repository.net.TripsRequest
import me.shkschneider.blablatrips.repository.net.TripsResponse

class DataManager(private val networkManager: NetworkManager) {

    @WorkerThread
    suspend fun trips(departure: String, arrival: String): Result<TripsResponse> =
        networkManager.trips(
            TripsRequest(
                departure = departure,
                arrival = arrival
            )
        ).toResult()

    private fun <T : Any> NetworkManager.Result<T>.toResult(): Result<T> =
        Result(data, error)

    data class Result<T : Any>(
        val data: T?,
        val error: Throwable?
    )

}